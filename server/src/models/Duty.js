const { Schema, model } = require('mongoose');


const dutySchema = new Schema({
	Name: {type: String, required: true}
}, {
	timestamps: true,
	versionKey: false
})

module.exports = model('Duty', dutySchema)